#[derive(PartialEq)]
pub struct State {
    pub light: Light,
    pub volume: Volume,
}

#[derive(Copy, Clone, PartialEq)]
#[repr(u8)]
pub enum Color { Red, Yellow, Blue, White, Off }

#[derive(Copy, Clone, PartialEq)]
#[repr(u8)]
pub enum Characteristic { Fixed, FlashingSystem, FlashingBluetooth, FadeOut }

#[derive(Copy, Clone, PartialEq)]
pub struct Light {
    pub color: Color,
    pub luminance: u8,
    pub characteristic: Characteristic,
}

impl std::fmt::Display for Light {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self.color {
            Color::Red => write!(f, "red@{}", self.luminance),
            Color::Yellow => write!(f, "yellow@{}", self.luminance),
            Color::Blue => write!(f, "blue@{}", self.luminance),
            Color::White => write!(f, "white@{}", self.luminance),
            Color::Off => write!(f, "off"),
        }
    }
}

impl From<Light> for String {
    fn from(item: Light) -> Self {
        match item.color {
            Color::Red => return format!("red@{}", item.luminance),
            Color::Yellow => return format!("yellow@{}", item.luminance),
            Color::Blue => return format!("blue@{}", item.luminance),
            Color::White => return format!("white@{}", item.luminance),
            Color::Off => return format!("off"),
        }
    }
}

#[derive(Copy, Clone, PartialEq)]
pub struct Volume {
    pub volume: u8,
    pub volume_changed: bool,
}

pub fn create(rx: std::sync::mpsc::Receiver<State>) -> std::thread::JoinHandle<()> {
    std::thread::spawn(move || {
        let mut print = false;
        let mut last_print = String::new();
        let mut last_flash = std::time::Instant::now();
        let mut state = State {
            light: Light { color: Color::Off, luminance: 0, characteristic: Characteristic::Fixed },
            volume: Volume { volume: 0, volume_changed: false },
        };

        loop {
            if print {
                match state.light.characteristic {
                    // flash with 1 hz
                    Characteristic::FlashingSystem => flash(1, &mut last_flash, &mut state, &mut print, &mut last_print),
                    // fade out to off over 3 seconds in 60 steps 
                    Characteristic::FadeOut => fade_out(3000, 60, &rx, &mut state, &mut print, &mut last_print),
                    // flash with 2 hz
                    Characteristic::FlashingBluetooth => flash(2, &mut last_flash, &mut state, &mut print, &mut last_print),
                    // continuous light (or off)
                    Characteristic::Fixed => fixed(&state, &mut print, &mut last_print),
                }
            }

            match rx.try_recv() {
                Ok(new_state) => { if new_state != state {
                    print = true;
                    state = new_state;
                } }
                Err(std::sync::mpsc::TryRecvError::Disconnected) => return,
                Err(std::sync::mpsc::TryRecvError::Empty) => {},
            }
        }
    })
}

fn fixed(state: &State, print: &mut bool, last_print: &mut String) {
    if String::from(state.light).ne(last_print) {
        *last_print = format!("{}", state.light);
        println!("{}", *last_print);
    }
    *print = false;
}

fn flash(frequency_hz: u32, last_flash: &mut std::time::Instant, state: &mut State, print: &mut bool, last_print: &mut String) {
    if last_flash.elapsed().as_millis() > (1_f32 / frequency_hz as f32 * 1000_f32) as u128 {
        println!("{}\noff", state.light);
        *last_print = "off".to_owned();
        *last_flash = std::time::Instant::now();
    }
    assert!(*print);
}

fn fade_out(length_ms: u32, steps: u32, rx: &std::sync::mpsc::Receiver<State>, state: &mut State, print: &mut bool, last_print: &mut String) {
    let start_volume = state.volume.volume;
    let increment = ((start_volume as f32) / (steps as f32)).ceil() as i32;
    for n in 0..(steps as i32 - 1) {
        match rx.try_recv() {
            Ok(new_state) => { if new_state != *state {
                    *state = new_state;
                    if state.volume.volume_changed {
                        fade_out(length_ms, steps, rx, state, print, last_print);
                        return;
                    }
                }
            }
            Err(std::sync::mpsc::TryRecvError::Disconnected) => return,
            Err(std::sync::mpsc::TryRecvError::Empty) => {}
        }

        let new_luminance = start_volume as i32 - n * increment;
        if new_luminance > 0 {
            state.light = Light { color: Color::White, luminance: new_luminance as u8, characteristic: Characteristic::Fixed };
            *last_print = format!("white@{}", new_luminance);
            println!("{}", *last_print);
        }
        else {
            state.light = Light { color: Color::Off, luminance: 0, characteristic: Characteristic::Fixed };
            *print = true;
            break;
        }

        std::thread::sleep(std::time::Duration::from_millis((length_ms / steps) as u64));
    }
}
