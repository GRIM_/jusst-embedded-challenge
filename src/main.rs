mod client;
mod embedded;
mod led;

const HOST_ADDRESS: &str = "wss://challenge.jusst.engineering/ws";

// for debugging
const PRINT_SERVER_MSG: bool = false;
const PRINT_DEVICE_STATE: bool = false;

fn main() {
    let (mut socket, _resp) = tungstenite::connect(url::Url::parse(HOST_ADDRESS).unwrap()).expect("connection failure");

    // client device states
    #[allow(unused_assignments)]
    let mut prev_client_ds = client::DeviceState::new();
    let mut curr_client_ds = client::DeviceState::new();

    // embedded device states
    #[allow(unused_assignments)]
    let mut prev_emb_ds = embedded::DeviceState::new();
    let mut curr_emb_ds = embedded::DeviceState::new();

    // The currently visible state of the led light - this is what will be printed to the console.
    let mut led_light = led::Light { color: led::Color::Off, luminance: 0, characteristic: led::Characteristic::Fixed };

    // Printing happens in a separate thread to support flashing and fade-out because we use a blocking call to receive a new message from the server.
    let (tx, rx): (std::sync::mpsc::Sender<led::State>, std::sync::mpsc::Receiver<led::State>) = std::sync::mpsc::channel();
    led::create(rx);

    loop {
        // The client state describes the full state of the device after every received message.
        // I use tungstenite to establish the connection to the server and serde to deserialize the json data. 
        prev_client_ds = curr_client_ds;
        curr_client_ds = client::get_device_state(&mut socket);

        if PRINT_DEVICE_STATE {
            println!("{:?}", curr_client_ds);
        }

        // The embedded state is a compressed (2 byte) version of the client states (most of the data is not needed for the embedded challenge). 
        prev_emb_ds = curr_emb_ds;
        curr_emb_ds = embedded::get_device_state(&curr_client_ds, &prev_client_ds, &led_light);

        // This is the "embedded" logic of the programm determining the new color of the led based on the received messages.
        // I tried to be quite efficient with this (low memory usage, few instructions) because I imagine that's what you have to do in embedded programming.
        led_light = embedded::control_led(&curr_emb_ds, &prev_emb_ds);

        // Send the new led state to the led thread for display.
        tx.send(
            led::State {
                light: led_light,
                volume: led::Volume {
                    volume: curr_client_ds.volume as u8,
                    volume_changed: curr_client_ds.volume != prev_client_ds.volume
                },
            }
        ).unwrap();
    }
}
