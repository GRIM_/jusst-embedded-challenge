use crate::client;
use crate::led;

pub struct DeviceState {
    pub flags: u8, // layout: lsb| system(2b) : playback(2b) : bluetooth(2b) : led_on(1b) : volume_changed(1b) |msb
    pub volume: u8,
}

impl DeviceState {
    pub fn new() -> Self {
        DeviceState {
            flags: 0,
            volume: 0,
        }
    }
}

pub fn get_device_state(curr_client_ds: &client::DeviceState, prev_client_ds: &client::DeviceState, led_light: &led::Light) -> DeviceState {
    DeviceState {
        flags: curr_client_ds.system as u8
            | (curr_client_ds.playback as u8) << 2
            | (curr_client_ds.bluetooth as u8) << 4
            | (!matches!(led_light.color, led::Color::Off) as u8) << 6
            | ((curr_client_ds.volume != prev_client_ds.volume) as u8) << 7,
        volume: curr_client_ds.volume as u8,
    }
}

pub fn control_led(curr_state: &DeviceState, prev_state: &DeviceState) -> led::Light
{
    let curr_system_state = get_system_state(curr_state.flags);
    if curr_system_state < client::SystemState::Ready as u8 {
        match curr_system_state.into() {
            client::SystemState::Booting => return led::Light { color: led::Color::Red, luminance: 10, characteristic: led::Characteristic::Fixed },
            client::SystemState::Error => return led::Light { color: led::Color::Red, luminance: 100, characteristic: led::Characteristic::Fixed },
            client::SystemState::Updating => {
                if is_led_on(curr_state.flags) && get_system_state(prev_state.flags) != client::SystemState::Updating as u8 {
                    return led::Light { color: led::Color::Off, luminance: 0, characteristic: led::Characteristic::Fixed };
                }
                return led::Light { color: led::Color::Yellow, luminance: 100, characteristic: led::Characteristic::FlashingSystem };
            }
            _ => {},
        }
    }

    if has_volume_changed(curr_state.flags) {
        return led::Light { color: led::Color::White, luminance: curr_state.volume, characteristic: led::Characteristic::FadeOut };
    }
    
    let curr_bluetooth_state = get_bluetooth_state(curr_state.flags);
    if curr_bluetooth_state == client::BluetoothState::Pairing as u8 {
        if is_led_on(curr_state.flags) && get_bluetooth_state(prev_state.flags) != client::BluetoothState::Pairing as u8 {
            return led::Light { color: led::Color::Off, luminance: 0, characteristic: led::Characteristic::Fixed };
        }
        return led::Light { color: led::Color::Blue, luminance: 100, characteristic: led::Characteristic::FlashingBluetooth };
    }

    match get_playback_state(curr_state.flags).into() {
        client::PlaybackState::Inactive => led::Light { color: led::Color::Off, luminance: 0, characteristic: led::Characteristic::Fixed },
        client::PlaybackState::Paused => return led::Light { color: led::Color::White, luminance: 50, characteristic: led::Characteristic::Fixed },
        client::PlaybackState::Playing => {
            if curr_bluetooth_state == client::BluetoothState::Connected as u8 {
                return led::Light { color: led::Color::Blue, luminance: 10, characteristic: led::Characteristic::Fixed };
            }
            return led::Light { color: led::Color::White, luminance: 10, characteristic: led::Characteristic::Fixed };
        },
    }
}

// bitmasks
const BM_SYSTEM       : u8 = 0b0000_0011;
const BM_PLAYBACK     : u8 = 0b0000_1100;
const BM_BLUETOOTH    : u8 = 0b0011_0000;
const BM_LED_ON       : u8 = 0b0100_0000;
const BM_VOLUME_CHANGE: u8 = 0b1000_0000;

#[inline]
fn get_system_state(flags: u8) -> u8 {
    flags & BM_SYSTEM
}

#[inline]
fn get_playback_state(flags: u8) -> u8 {
    (flags & BM_PLAYBACK) >> 2
}

#[inline]
fn get_bluetooth_state(flags: u8) -> u8 {
    (flags & BM_BLUETOOTH) >> 4
}

#[inline]
fn is_led_on(flags: u8) -> bool {
    (flags & BM_LED_ON) >> 6 != 0
}

#[inline]
fn has_volume_changed(flags: u8) -> bool {
    (flags & BM_VOLUME_CHANGE) >> 7 != 0
}
