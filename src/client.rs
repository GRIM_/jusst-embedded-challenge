#[derive(Debug, Copy, Clone, serde::Deserialize)]
#[serde(rename_all = "lowercase")]
#[repr(u8)]
pub enum SystemState { Booting, Updating, Error, Ready }

impl From<u8> for SystemState {
    fn from(item: u8) -> Self {
        match item {
            0 => return SystemState::Booting,
            1 => return SystemState::Updating,
            2 => return SystemState::Error,
            3 => return SystemState::Ready,
            _ => {},
        }
        SystemState::Booting
    }
}

#[derive(Debug, Copy, Clone, serde::Deserialize)]
#[serde(rename_all = "lowercase")]
#[repr(u8)]
pub enum PlaybackState { Inactive, Paused, Playing }

impl From<u8> for PlaybackState {
    fn from(item: u8) -> Self {
        match item {
            0 => return PlaybackState::Inactive,
            1 => return PlaybackState::Paused,
            2 => return PlaybackState::Playing,
            _ => {},
        }
        PlaybackState::Inactive
    }
}

#[derive(Debug, Copy, Clone, serde::Deserialize)]
#[serde(rename_all = "lowercase")]
#[repr(u8)]
pub enum BluetoothState { Inactive, Pairing, Connected }

impl From<u8> for BluetoothState {
    fn from(item: u8) -> Self {
        match item {
            0 => return BluetoothState::Inactive,
            1 => return BluetoothState::Pairing,
            2 => return BluetoothState::Connected,
            _ => {},
        }
        BluetoothState::Inactive
    }
}

#[derive(Debug, Clone, serde::Deserialize)]
#[serde(default = "clear_metadata")]
pub struct MetaData {
  pub title: String,
  pub artist: String,
  #[serde(rename(deserialize = "coverArt"))]
  pub cover_art: String,
  pub duration: u32,
}

fn clear_metadata() -> MetaData { MetaData{ title: String::new(), artist: String::new(), cover_art: String::new(), duration: 0 } }

// Using a static struct seems to be the only way to let serde use the previously deserialized values for omitted json fields.
// Using those fields requires an unsafe block but I assume that's just due to potential issues with multiple threads accessing the data (which we are not doing).
pub static mut SAVED_STATE: DeviceState = DeviceState {
    system: SystemState::Booting,
    playback: PlaybackState::Inactive,
    volume: 50,
    bluetooth: BluetoothState::Inactive,
    metadata: MetaData{ title: String::new(), artist: String::new(), cover_art: String::new(), duration: 0 },
    playback_position: 0,
};

fn prev_system_state() -> SystemState { unsafe { SAVED_STATE.system } }
fn prev_playback_state() -> PlaybackState { unsafe { SAVED_STATE.playback } }
fn prev_volume() -> u32 { unsafe { SAVED_STATE.volume } }
fn prev_bluetooth_state() -> BluetoothState { unsafe { SAVED_STATE.bluetooth } }
fn prev_metadata() -> MetaData { unsafe { MetaData {
    title: SAVED_STATE.metadata.title.clone(),
    artist: SAVED_STATE.metadata.artist.clone(),
    cover_art: SAVED_STATE.metadata.cover_art.clone(),
    duration: SAVED_STATE.metadata.duration.clone(),
} } }
fn prev_playback_position() -> u32 { unsafe { SAVED_STATE.playback_position } }

#[derive(Debug, Clone, serde::Deserialize)]
pub struct DeviceState {
    #[serde(default = "prev_system_state")]
    pub system: SystemState,
    #[serde(default = "prev_playback_state")]
    pub playback: PlaybackState,
    #[serde(default = "prev_volume")]
    pub volume: u32,
    #[serde(default = "prev_bluetooth_state")]
    pub bluetooth: BluetoothState,
    #[serde(default = "prev_metadata")]
    pub metadata: MetaData,
    #[serde(default = "prev_playback_position", rename(deserialize = "playbackPosition"))]
    pub playback_position: u32,
}

impl DeviceState {
    pub fn new() -> Self {
        DeviceState {
            system: SystemState::Booting,
            playback: PlaybackState::Inactive,
            volume: 50,
            bluetooth: BluetoothState::Inactive,
            metadata: MetaData { title: String::new(), artist: String::new(), cover_art: String::new(), duration: 0 },
            playback_position: 0,
        }
    }
}

pub fn get_device_state(socket: &mut tungstenite::WebSocket<tungstenite::stream::MaybeTlsStream<std::net::TcpStream>>) -> DeviceState {
    let msg = socket.read_message().expect("error reading message");
    let parsed_msg: serde_json::Value = serde_json::from_str(&(|msg| {
        let text_msg = match msg {
            tungstenite::Message::Text(msg) => { msg }
            _ => { String::from("not a text message") }
        };
        text_msg
    })(msg)).expect("json malformed");
    
    let new_state: DeviceState = serde_json::from_str(&parsed_msg.to_string()).unwrap();
    unsafe { SAVED_STATE = new_state.clone(); }
    
    if crate::PRINT_SERVER_MSG {
        println!("Parsed Msg  {}", parsed_msg);
    }

    new_state
}
