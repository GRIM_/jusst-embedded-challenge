## Description

This is a solution to the embedded challenge outlined here: https://challenge.jusst.engineering

Run with:
```sh
cargo build --release
./target/release/jusst_embedded_challenge
```

The client will try to connect to `wss://challenge.jusst.engineering/ws` by default.
